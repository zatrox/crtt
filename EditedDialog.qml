import QtQuick 6.0
import QtQuick.Controls 6.0
import QtQuick.Layouts 6.0

Dialog {
    standardButtons: Dialog.Apply | Dialog.Cancel
    modal: true
    onApplied: {
        delegate.station = stationField.text
        delegate.trainId = trainIdField.text
        delegate.line = lineField.text
        delegate.track = trackField.text
        delegate.arrival = arrivalField.text
        delegate.departure = departureField.text
        delegate.comment = commentField.text
        close()
    }
    property QtObject delegate

    GridLayout {
        columns: 2
        Label {
            text: "Station"
        }
        TextField {
            id: stationField
            text: delegate.station
        }
        Label {
            text: "TrainId"
        }
        TextField {
            id: trainIdField
            text: delegate.trainId
        }
        Label {
            text: "Line"
        }
        TextField {
            id: lineField
            text: delegate.line
        }
        Label {
            text: "Track"
        }
        TextField {
            id: trackField
            text: delegate.track
        }
        Label {
            text: "Arrival"
        }
        TextField {
            id: arrivalField
            text: delegate.arrival
        }
        Label {
            text: "Departure"
        }
        TextField {
            id: departureField
            text: delegate.departure
        }
        Label {
            text: "Comment"
        }
        TextField {
            id: commentField
            text: delegate.comment
        }
    }
}
