#ifndef TIMETABLEMODEL_H
#define TIMETABLEMODEL_H

#include <QAbstractListModel>
#include "Timetable.h"
#include <qqml.h>

class TimetableModel : public QAbstractListModel
{
    Q_OBJECT
    QML_ELEMENT
public:

    enum Roles {
        Station,
        TrainId,
        Line,
        Track,
        Arrival,
        Departure,
        Comment
    };

    explicit TimetableModel(QObject *parent = nullptr);

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    Q_INVOKABLE bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;

    QHash<int, QByteArray> roleNames() const override;

    Q_INVOKABLE void addRow();

private:
    Timetable m_timetable;
};

#endif // TIMETABLEMODEL_H
