#ifndef TIMETABLE_H
#define TIMETABLE_H

#include <QList>
#include <QTime>

class TimetableEntry
{
public:
    TimetableEntry();

    QString station() const;
    void setStation(const QString &station);

    QString trainId() const;
    void setTrainId(const QString &trainId);

    QString line() const;
    void setLine(const QString &line);

    QString track() const;
    void setTrack(const QString &track);

    QTime arrival() const;
    void setArrival(const QTime &arrival);

    QTime departure() const;
    void setDeparture(const QTime &departure);

    QString comment() const;
    void setComment(const QString &comment);

private:
    QString m_station;
    QString m_trainId;
    QString m_line;
    QString m_track;
    QTime m_arrival;
    QTime m_departure;
    QString m_comment;
};

class Timetable
{
public:
    Timetable();
    QList<TimetableEntry> entries() const;
    QList<TimetableEntry> &entries();
    void setEntries(const QList<TimetableEntry> &entries);

private:
    QList<TimetableEntry> m_entries;
};

#endif // TIMETABLE_H
