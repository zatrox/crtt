#include "Timetable.h"

TimetableEntry::TimetableEntry()
{

}

QString TimetableEntry::station() const
{
    return m_station;
}

void TimetableEntry::setStation(const QString &station)
{
    m_station = station;
}

QString TimetableEntry::trainId() const
{
    return m_trainId;
}

void TimetableEntry::setTrainId(const QString &trainId)
{
    m_trainId = trainId;
}

QString TimetableEntry::line() const
{
    return m_line;
}

void TimetableEntry::setLine(const QString &line)
{
    m_line = line;
}

QString TimetableEntry::track() const
{
    return m_track;
}

void TimetableEntry::setTrack(const QString &track)
{
    m_track = track;
}

QTime TimetableEntry::arrival() const
{
    return m_arrival;
}

void TimetableEntry::setArrival(const QTime &arrival)
{
    m_arrival = arrival;
}

QTime TimetableEntry::departure() const
{
    return m_departure;
}

void TimetableEntry::setDeparture(const QTime &departure)
{
    m_departure = departure;
}

QString TimetableEntry::comment() const
{
    return m_comment;
}

void TimetableEntry::setComment(const QString &comment)
{
    m_comment = comment;
}

Timetable::Timetable()
{

}

QList<TimetableEntry> Timetable::entries() const
{
  return m_entries;
}

QList<TimetableEntry> &Timetable::entries()
{
    return m_entries;
}

void Timetable::setEntries(const QList<TimetableEntry> &entries)
{
    m_entries = entries;
}
