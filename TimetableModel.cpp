#include "TimetableModel.h"

QTime timeFromUserInput(const QVariant &value)
{
    const auto string = value.toString();
    if (string.contains(u':')) {
        return QTime::fromString(string, u"HH:mm");
    }
    return QTime::fromString(string, u"HHmm");
}

TimetableModel::TimetableModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

int TimetableModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid())
        return 0;

    return m_timetable.entries().size();
}

QVariant TimetableModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    const auto entry = m_timetable.entries().at(index.row());

    switch (role) {
    case Station:
        return entry.station();
    case TrainId:
        return entry.trainId();
    case Line:
        return entry.line();
    case Track:
        return entry.track();
    case Arrival:
        return entry.arrival().toString(u"HH:mm");
    case Departure:
        return entry.departure().toString(u"HH:mm");
    case Comment:
        return entry.comment();
    }

    return QVariant();
}

bool TimetableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid())
        return false;

    auto &entry = m_timetable.entries()[index.row()];

    qDebug() << role << Roles(role) << value.typeName() << value;
    switch (role) {
    case Station:
        entry.setStation(value.toString());
        break;
    case TrainId:
        entry.setTrainId(value.toString());
        break;
    case Line:
        entry.setLine(value.toString());
        break;
    case Track:
        entry.setTrack(value.toString());
        break;
    case Arrival:
        entry.setArrival(timeFromUserInput(value));
        break;
    case Departure:
        entry.setDeparture(timeFromUserInput(value));
        break;
    case Comment:
        entry.setComment(value.toString());
        break;
    default:
        return false;
    }
    emit dataChanged(index, index, {role});
    return true;
}

QHash<int, QByteArray> TimetableModel::roleNames() const
{
    return {
        {Station, "station"},
        {TrainId, "trainId"},
        {Line, "line"},
        {Track, "track"},
        {Arrival, "arrival"},
        {Departure, "departure"},
        {Comment, "comment"}
    };
}

void TimetableModel::addRow()
{
    beginInsertRows(QModelIndex(), m_timetable.entries().size(), m_timetable.entries().size());
    m_timetable.entries().append(TimetableEntry());
    endInsertRows();
}
