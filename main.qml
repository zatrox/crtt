import QtQuick 6.0
import QtQuick.Window 6.0
import QtQuick.Controls 6.0
import QtQuick.Layouts 6.0

import CRTT

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")
    color: SystemPalette.dark

    EditedDialog {
        id: editedDialog
        x: (parent.width - width)/2
        y: (parent.height - height)/2
    }

    ListView {
        model: TimetableModel {
            id: timetablemodel
        }
        anchors.fill: parent
        delegate: RowLayout {
            id: itemDelegate

            required property QtObject model

            required property string station
            required property string trainId
            required property string line
            required property string track
            required property var arrival
            required property var departure
            required property string comment

            Label {
                text: station
            }
            Label {
                text: trainId
            }
            Label {
                text: line
            }
            Label {
                text: track
            }
            Label {
                text: arrival
            }
            Label {
                text: departure
            }
            Label {
                text: comment
            }

            Button {
                text: "Edit"
                onClicked: {
                    editedDialog.open()
                    editedDialog.delegate = model
                }
            }
        }
        header: Button {
            text: "Add"
            onClicked: timetablemodel.addRow()
        }
    }
}
